import { chain, isUndefined } from 'underscore';

import dropdownTemplate from './dropdown.html';
import dropdownEditorTemplate from './edit-dropdown.html';

function DropdownRenderer() {
  return {
    restrict: 'E',
    scope: {
      queryResult: '=',
      options: '=',
    },
    template: dropdownTemplate,
    link($scope) {
      const refreshData = () => {
        const data = $scope.queryResult.getData();

        if (!data) {
          return;
        }

        $scope.options.displayingColumnName = isUndefined($scope.options.displayingColumnName)
          ? $scope.options.dataColumnName
          : $scope.options.displayingColumnName;

        $scope.options.resultSet = chain(data)
          .pluck($scope.options.displayingColumnName)
          .uniq()
          .value();
      };

      $scope.selectValue = () => {
        $scope.options.realValue = chain($scope.queryResult.getData())
          .find({ [$scope.options.displayingColumnName]: $scope.options.selectedValue })
          .result($scope.options.dataColumnName)
          .value() || null;

        $scope.$root.$broadcast('dropdown:selected', {
          name: $scope.options.paramName,
          value: $scope.options.realValue,
        });
      };

      $scope.$watch('options', refreshData, true);
      $scope.$watch('queryResult && queryResult.getData()', refreshData);
    },
  };
}

function DropdownEditor() {
  return {
    restrict: 'E',
    scope: {
      queryResult: '=',
      options: '=',
    },
    template: dropdownEditorTemplate,
    link($scope) {
      $scope.resetDropdownValue = () => {
        $scope.options.selectedValue = $scope.options.realValue = '';
      };

      function refreshColumnsList() {
        $scope.columnsList = $scope.queryResult.getColumnNames();
        $scope.resetDropdownValue();
      }

      $scope.$watch('queryResult && queryResult.getData()', refreshColumnsList);
    },
  };
}

export default function (ngModule) {
  ngModule.directive('dropdownRenderer', DropdownRenderer);
  ngModule.directive('dropdownEditor', DropdownEditor);

  ngModule.config((VisualizationProvider) => {
    const renderTemplate =
      '<dropdown-renderer ' +
      'options="visualization.options" query-result="queryResult">' +
      '</dropdown-renderer>';

    const editTemplate = '<dropdown-editor options="visualization.options" ' +
      'query-result="queryResult"></dropdown-editor>';

    VisualizationProvider.registerVisualization({
      type: 'DROPDOWN',
      name: 'Dropdown',
      renderTemplate,
      editorTemplate: editTemplate,
    });
  });
}
